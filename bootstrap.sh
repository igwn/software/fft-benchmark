#! /bin/bash

mkdir -p build
cd build

export CC=clang
export CXX=clang++

cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/../install -DPLOTTER=ON -DCMAKE_BUILD_TYPE="Release" \
         -DFFTW="$FFTW" \
         -DKFR5="$KFR5" -DKFR6="$KFR6" \
         -DIPP="$IPP" \
         -DMKL="$MKL"\

make -j8 && make install
cd ..
