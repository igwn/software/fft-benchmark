.PHONY: benchmark benchmark-smalln benchmark-highn benchmark-potn

PATH := ${PATH}:./install/bin

benchmark: benchmark-cazarin benchmark-smalln benchmark-highn benchmark-potn
plot: plot-cazarin plot-smalln plot-highn plot-potn

benchmark-cazarin:
	@scripts/fftcazarin share/fft-benchmark.$(shell arch).cazarin kfr5
	@scripts/fftcazarin share/fft-benchmark.$(shell arch).cazarin kfr6
	@scripts/fftcazarin share/fft-benchmark.$(shell arch).cazarin fftw
	@scripts/fftcazarin share/fft-benchmark.$(shell arch).cazarin ipp
	@scripts/fftcazarin share/fft-benchmark.$(shell arch).cazarin mkl

plot-cazarin:
	@plotter share/fft-benchmark.$(shell arch).cazarin.*.json || true

benchmark-smalln:
	@scripts/fftdivision share/fft-benchmark.$(shell arch).smalln kfr5 1000 100
	@scripts/fftdivision share/fft-benchmark.$(shell arch).smalln kfr6 1000 100
	@scripts/fftdivision share/fft-benchmark.$(shell arch).smalln fftw 1000 100
	@scripts/fftdivision share/fft-benchmark.$(shell arch).smalln ipp 1000 100
	@scripts/fftdivision share/fft-benchmark.$(shell arch).smalln mkl 1000 100

plot-smalln:
	@plotter share/fft-benchmark.$(shell arch).smalln.*.json || true

benchmark-highn:
	@scripts/fftdivision share/fft-benchmark.$(shell arch).highn kfr5 1000000 100
	@scripts/fftdivision share/fft-benchmark.$(shell arch).highn kfr6 1000000 100
	@scripts/fftdivision share/fft-benchmark.$(shell arch).highn fftw 1000000 100
	@scripts/fftdivision share/fft-benchmark.$(shell arch).highn ipp 1000000 100
	@scripts/fftdivision share/fft-benchmark.$(shell arch).highn mkl 1000000 100

plot-highn:
	@plotter share/fft-benchmark.$(shell arch).highn.*.json || true

benchmark-potn:
	@scripts/fftpot share/fft-benchmark.$(shell arch).potn kfr5 24
	@scripts/fftpot share/fft-benchmark.$(shell arch).potn kfr6 24
	@scripts/fftpot share/fft-benchmark.$(shell arch).potn fftw 24
	@scripts/fftpot share/fft-benchmark.$(shell arch).potn ipp 24
	@scripts/fftpot share/fft-benchmark.$(shell arch).potn mkl 24

plot-potn:
	@plotter share/fft-benchmark.$(shell arch).potn.*.json || true
