FROM ubuntu:latest

# Update package lists and install necessary packages
ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV NPROC 8
ENV CC clang
ENV CXX clang++

COPY Dockerfile.packages packages

RUN apt-get update -qq && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime && \
    apt-get -y install $(cat packages | grep -v '#') && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/*

ENV LD_LIBRARY_PATH /usr:/usr/lib:/usr/lib/aarch64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/local/lib

# FFTW3 (Optional)
RUN wget -q https://www.fftw.org/fftw-3.3.10.tar.gz
RUN tar -xzvf fftw-3.3.10.tar.gz
RUN cd fftw-3.3.10 && \
    if [ ! -z "$(arch | grep -E '^(arm|aarch)')" ]; then \
        ./configure --enable-neon --enable-mpi --enable-threads --enable-shared --enable-float --prefix=/opt/fftw/3.3.10; \
    else \
        ./configure --enable-avx --enable-sse2 --enable-mpi --enable-threads --enable-shared --enable-float --prefix=/opt/fftw/3.3.10; \
    fi

RUN cd fftw-3.3.10 && make -j${NPROC} && make install
RUN rm -rf fftw-3.3.10.tar.gz fftw-3.3.10
ENV FFTW /opt/fftw/3.3.10

# KFR5/6
RUN wget -q https://github.com/kfrlib/kfr/archive/refs/tags/6.0.2.tar.gz
RUN tar -xzvf 6.0.2.tar.gz
RUN cd kfr-6.0.2 && mkdir build
RUN cd kfr-6.0.2/build && cmake .. -DKFR_ENABLE_CAPI_BUILD=ON -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DCMAKE_INSTALL_PREFIX=/opt/kfr/6.0.2
RUN cd kfr-6.0.2/build && make -j${NPROC} && make install
RUN rm -rf 6.0.2.tar.gz kfr-6.0.2
ENV KFR6 /opt/kfr/6.0.2

# RUN wget -q https://github.com/kfrlib/kfr/archive/refs/tags/5.1.0.tar.gz
# RUN tar -xzvf 5.1.0.tar.gz
# RUN cd kfr-5.1.0 && mkdir build
# RUN cd kfr-5.1.0/build && cmake .. -DKFR_ENABLE_CAPI_BUILD=ON -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DCMAKE_INSTALL_PREFIX=/opt/kfr/5.1.0
# RUN cd kfr-5.1.0/build && make -j${NPROC} && make install
# RUN rm -rf 5.1.0.tar.gz kfr-5.1.0
# Not tested yet
ENV KFR5 false

# Intel IPP (Optional)
# RUN wget -q https://registrationcenter-download.intel.com/akdlm/IRC_NAS/046b1402-c5b8-4753-9500-33ffb665123f/l_ipp_oneapi_p_2021.10.1.16.sh
# RUN chmod 755 l_ipp_oneapi_p_2021.10.1.16.sh
# RUN if [ ! -z "$(arch | grep -E '^(arm|aarch)')" ]; then \
#         ./l_ipp_oneapi_p_2021.10.1.16.sh -a --cli --silent; \
#     fi
# RUN rm ./l_ipp_oneapi_p_2021.10.1.16.sh
# Not tested yet
ENV IPP false

# Intel MKL (Optional)
# RUN wget -q https://registrationcenter-download.intel.com/akdlm/IRC_NAS/adb8a02c-4ee7-4882-97d6-a524150da358/l_onemkl_p_2023.2.0.49497_offline.sh
# RUN chmod 755 ./l_onemkl_p_2023.2.0.49497_offline.sh
# RUN if [ ! -z "$(arch | grep -E '^(arm|aarch)')" ]; then \
#         ./l_onemkl_p_2023.2.0.49497_offline.sh -a --cli --silent --eula; \
#     fi
# RUN rm ./l_onemkl_p_2023.2.0.49497_offline.sh
# Not tested yet
ENV MKL false

CMD ["bash"]
