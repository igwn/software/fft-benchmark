# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

# Load using standard package finder
set(_KFR_CAPI kfr_capi)
set(_KFR_DFT kfr_dft)
set(_KFR_DSP kfr_dsp)

find_package_standard(
  NAMES kfr_io
  OPTIONALS ${_KFR_CAPI} ${_KFR_DFT} ${_KFR_DSP}
  HEADERS "kfr/kfr.h"
  PATHS ${KFR5} $ENV{KFR5}
)
