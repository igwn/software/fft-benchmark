# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

# Load using standard package finder
set(_KFR_CAPI kfr_capi)
set(_KFR_DFT kfr_dft_neon64 libkfr_dft_sse2 libkfr_dft_avx512 libkfr_dft_avx2 libkfr_dft_avx libkfr_dft_sse41 libkfr_dft_sse2)
set(_KFR_DSP kfr_dsp_neon64 libkfr_dsp_sse2 libkfr_dsp_avx512 libkfr_dsp_avx2 libkfr_dsp_avx libkfr_dsp_sse41 libkfr_dsp_sse2)

find_package_standard(
  NAMES kfr_io
  OPTIONALS ${_KFR_CAPI} ${_KFR_DFT} ${_KFR_DSP}
  HEADERS "kfr/kfr.h"
  PATHS ${KFR6} $ENV{KFR6}
)
