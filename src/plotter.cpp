#include <Riostream.h>
#include <fstream>
#include <vector>
#include <string>
#include <nlohmann/json.hpp>
#include <TApplication.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TImage.h>
#include <TMath.h>
#include <TROOT.h>
#include <TFile.h>

bool extendPlots = false;

using TJson = nlohmann::json;
class BenchmarkEntry
{
    protected:

        TString cpu;
        TString library;
        TString title;
        
        TString direction;
        TString precision;
        TString type;
        TString buffer;

        std::vector<int> sizes;
        std::vector<double> mflops;
        std::vector<double> best_times;
        std::vector<double> avg_times;
        std::vector<int> calls;

        template <typename T, typename Ti>
        static inline void Reorder(std::vector<T>& v, std::vector<Ti> indexes)  
        {
            assert(v.size() == indexes.size());

            std::vector<T> temp(v.size());
            for(size_t i = 0; i < indexes.size(); ++i) {
                assert(indexes[i] < v.size());
                temp[i] = v[indexes[i]];
            }
            
            v = std::move(temp);
        }

    public:

        BenchmarkEntry(TString title, const TJson& root): title(title)
        {
            cpu     = TString(root["cpu"]).ReplaceAll("(", "").ReplaceAll(")", "");
            library = TString(root["library"]);
            
            Add(root);
        }

        TString GetTitle() { return title; }
        TString GetDirection() { return direction; }
        TString GetBuffer() { return buffer; }
        TString GetPrecision() { return precision; }
        TString GetType() { return type; }

        TString GetLibrary() { return library; }
        TString GetCPU() { return cpu; }

        int Add(const TJson& root) {

            TString _cpu     = TString(root["cpu"]).ReplaceAll("(", "").ReplaceAll(")", "");
            TString _library = TString(root["library"]);

            for (const auto &result : root["results"])
            {
                int _nDims = result["size"].is_array() ? result["size"].size() : 1;
                TString _title   = (TString) result["data"] + "-" + result["type"] + "-" + result["direction"] + "-" + result["buffer"]+ "-dim" + TString::Itoa(_nDims, 10);
                if(_nDims > 1) {
                    
                    // std::cerr << _title << ": 2D plot not handled yet." << std::endl;
                    continue;
                }

                if(!this->IsCompatible(_cpu, _library, _title)) continue;
                if(result.find("error") != result.end()) {
                    continue;
                }

                direction = TString(result["direction"]);
                precision = TString(result["data"]);
                type = TString(result["type"]);
                buffer = TString(result["buffer"]);

                int index = std::find(sizes.begin(), sizes.end(), result["size"]) - sizes.begin();
                if(index == this->GetN()) {

                    sizes.resize(index+1);
                    mflops.resize(index+1);
                    best_times.resize(index+1);
                    avg_times.resize(index+1);
                    calls.resize(index+1);
                }

                this->sizes[index] = result["size"];
                this->mflops[index] = result["mflops"];
                this->best_times[index] = result["best_time"];
                this->avg_times[index] = result["avg_time"];
                this->calls[index] = result["calls"];
            }
            
            this->Sort();
            return true;
        }

        int Add(BenchmarkEntry& entry) {

            if(!this->IsCompatible(entry)) return false;

            for(int i = 0, N = entry.GetN(); i < N; i++) {

                int index = std::find(sizes.begin(), sizes.end(), entry.sizes[i]) - sizes.begin();
                if(index == this->GetN()) {

                    sizes.resize(index+1);
                    mflops.resize(index+1);
                    best_times.resize(index+1);
                    avg_times.resize(index+1);
                    calls.resize(index+1);
                }

                this->sizes[index] = entry.sizes[i];
                this->mflops[index] = entry.mflops[i];
                this->best_times[index] = entry.best_times[i];
                this->avg_times[index] = entry.avg_times[i];
                this->calls[index] = entry.calls[i];
            }

            this->Sort();
            return true;
        }

        void Sort()
        {
            std::vector<int> indices;
            for(int i = 0, N = this->sizes.size(); i < N; i++)
                indices.push_back(i);

            std::iota(indices.begin(), indices.end(), 0);
            std::sort(indices.begin(), indices.end(), [&](int A, int B) -> bool { return this->sizes[A] < this->sizes[B]; });

            BenchmarkEntry::Reorder(this->sizes, indices);
            BenchmarkEntry::Reorder(this->mflops, indices);
            BenchmarkEntry::Reorder(this->best_times, indices);
            BenchmarkEntry::Reorder(this->avg_times, indices);
            BenchmarkEntry::Reorder(this->calls, indices);
        }

        TString GetID() { return (TString) cpu + "; " + library + "; " + title; }
        inline bool IsCompatible(BenchmarkEntry &benchmark) { return this->GetID().EqualTo(benchmark.GetID()); }
        inline bool IsCompatible(TString cpu, TString library, TString title = "") { return this->GetID().BeginsWith(TString(cpu).ReplaceAll("(","").ReplaceAll(")","") + "; " + library + "; " + title); }
        inline bool IsCompatible(TString title) { return IsCompatible(this->cpu, this->library, title); }

        int GetN() { return sizes.size(); }
        int GetMaxSize() { return sizes.size() > 0 ? sizes[sizes.size()-1] : 0; }

        std::vector<int> GetX() { return sizes; }

        void Print() {
            
            std::cout << "* contains `" << title << "` with " << this->GetN() << " data points" << std::endl;
        }

        TH1 *GetMFlops(Color_t color = kBlue+2)
        {
            if(sizes.size() < 1) return NULL;

            TH1D *h = new TH1D("hMFlops_"+this->GetTitle()+"["+this->GetCPU()+"]["+this->GetLibrary()+"]", this->GetTitle() + ";FFT size;MFlops", sizes.size(), 0, sizes.size()-1);
            for(int i = 0, N = sizes.size(); i < N; i++) {

                if(sizes[i] == 0) continue;
                h->SetBinContent(i+1, mflops[i]);
                h->GetXaxis()->SetBinLabel(i+1, TString::Itoa(sizes[i],10) );
            }

            h->GetXaxis()->SetTitleOffset(2.15);
            h->GetXaxis()->LabelsOption("v");
            h->GetYaxis()->SetMaxDigits(3);
            h->SetMarkerStyle(86);
            h->SetMarkerSize(2);
            h->SetMarkerColor(color);
            h->SetLineWidth(2);
            h->SetLineColor(color);

            return h;
        }

        TH1 *GetAvgTime(Color_t color = kBlue+2)
        {
            if(sizes.size() < 1) return NULL;

            TH1D *h = new TH1D("hAvgTime_"+this->GetTitle()+"["+this->GetCPU()+"]["+this->GetLibrary()+"]", this->GetTitle()+ ";FFT size; Average time (in #mus)", sizes.size(), 0, sizes.size()-1);
            for(int i = 0, N = sizes.size(); i < N; i++) {
                if(sizes[i] == 0) continue;
                h->SetBinContent(i+1, avg_times[i]);
                h->GetXaxis()->SetBinLabel(i+1, TString::Itoa(sizes[i],10) );
            }

            h->GetXaxis()->SetTitleOffset(2.15);
            h->GetXaxis()->LabelsOption("v");
            h->GetYaxis()->SetMaxDigits(3);

            h->SetMarkerStyle(86);
            h->SetMarkerSize(2);
            h->SetMarkerColor(color);
            h->SetLineWidth(2);
            h->SetLineColor(color);

            return h;
        }
        TH1 *GetBestTime(Color_t color = kBlue+2)
        {
            if(sizes.size() < 1) return NULL;

            TH1D *h = new TH1D("hBestTime_"+this->GetTitle()+"["+this->GetCPU()+"]["+this->GetLibrary()+"]", this->GetTitle()+ ";FFT size; Best time (in #mus)", sizes.size(), 0, sizes.size()-1);
            for(int i = 0, N = sizes.size(); i < N; i++) {
                if(sizes[i] == 0) continue;
                h->SetBinContent(i+1, best_times[i]);
                h->GetXaxis()->SetBinLabel(i+1, TString::Itoa(sizes[i],10) );
            }

            h->GetXaxis()->SetTitleOffset(2.15);
            h->GetXaxis()->LabelsOption("v");
            h->GetYaxis()->SetMaxDigits(3);

            h->SetMarkerStyle(86);
            h->SetMarkerSize(2);
            h->SetMarkerColor(color);
            h->SetLineWidth(2);
            h->SetLineColor(color);

            return h;
        }
        TH1 *GetNCalls(Color_t color = kBlue+2)
        {
            if(sizes.size() < 1) return NULL;

            TH1D *h = new TH1D("hNCalls_"+this->GetTitle()+"["+this->GetCPU()+"]["+this->GetLibrary()+"]", this->GetTitle()+ "; FFT size; Number of iterations", sizes.size(), 0, sizes.size()-1);
            for(int i = 0, N = sizes.size(); i < N; i++) {
                if(sizes[i] == 0) continue;
                h->SetBinContent(i+1, calls[i]);
                h->GetXaxis()->SetBinLabel(i+1, TString::Itoa(sizes[i],10) );
            }

            h->GetXaxis()->SetTitleOffset(2.15);
            h->GetXaxis()->LabelsOption("v");
            h->GetYaxis()->SetMaxDigits(3);

            h->SetMarkerStyle(86);
            h->SetMarkerSize(2);
            h->SetMarkerColor(color);
            h->SetLineWidth(2);
            h->SetLineColor(color);

            return h;
        }
};

class Benchmark
{
    protected:

        TString cpu;
        TString library;
        std::vector<BenchmarkEntry*> entries;
        
        Color_t color;

    public:

        ~Benchmark() {

            for(auto entry : entries) {

                delete entry;
            }

            entries.clear();
        }

        Benchmark(const TJson& root, Color_t color = kBlue+2): color(color) {

            cpu     = TString(root["cpu"]).ReplaceAll("(", "").ReplaceAll(")", "");
            library = TString(root["library"]);

            AddEntry(root);
        }

        TString GetCPU() { return cpu; }
        TString GetLibrary() { return library; }
        TString GetID() { return (TString) cpu + "; " + library; }
        int GetMaxSize() { 

            int N = 0;
            for ( auto *entry : entries )
                N = TMath::Max(N, entry->GetMaxSize());

            return N;
        };

        inline bool IsCompatible(Benchmark &benchmark) { return this->GetID().EqualTo(benchmark.GetID()); }
        inline bool IsCompatible(TString cpu, TString library) { return this->GetID().EqualTo(TString(cpu).ReplaceAll("(","").ReplaceAll(")","") + "; " + library); }

        Color_t GetColor() { return color; }

        BenchmarkEntry* GetEntry(TString cpu, TString library, TString title = "") 
        {
            for ( auto *entry : entries )
            {
                if( entry && entry->IsCompatible(cpu, library, title) ) 
                    return entry;
            }

            return NULL;
        }

        std::vector<BenchmarkEntry*> GetEntries(TString title = "") 
        {
            std::vector<BenchmarkEntry*> _entries;
            for ( auto *entry : entries )
            {
                if( entry && entry->IsCompatible(cpu, library, title) )
                    _entries.push_back(entry);
            }

            return _entries;
        }

        bool AddEntry(const TJson &root)
        {
            if(!this->IsCompatible(root["cpu"], root["library"])) return false;

            BenchmarkEntry *entry = NULL;
            for (const auto &result : root["results"])
            {
                int _nDims = result["size"].is_array() ? result["size"].size() : 1;
                TString title = (TString) result["data"] + "-" + result["type"] + "-" + result["direction"] + "-" + result["buffer"]+ "-dim" + TString::Itoa(_nDims, 10);
                entry = this->GetEntry(root["cpu"], root["library"], title);

                if(entry) entry->Add(root); 
                else {

                    entry = new BenchmarkEntry(title, root);
                    entries.push_back(entry);
                }
            }

            return true;
        }

        bool AddEntry(Benchmark &benchmark) {

            if(!this->IsCompatible(benchmark)) return false;

            for ( auto *_entry : benchmark.entries ) {

                if(_entry == NULL) continue;

                bool found = false;
                for ( auto *entry : this->entries ) {

                    found = entry->Add(*_entry);
                    if(found) break;
                }
            }
            
            return true;
        }

        void Print() {

            std::cout << "Benchmark on CPU `" << cpu << "` with library `" << library << "`" << std::endl;
            for ( auto *entry : this->entries ) {
                entry->Print();
            }

            std::cout << std::endl;
        }
        
        std::vector<TString> GetTitles() {

            std::vector<TString> titles;
            for ( auto *entry : this->entries ) {
                titles.push_back(entry->GetTitle());
            }

            return titles;
        }
};

TLegend *GetLegendFromCurrentPad()
{
    TLegend *legend = nullptr;
    TObject *obj;
    TIter next(gPad->GetListOfPrimitives());
    while ((obj = next())) {
        if (obj->InheritsFrom("TLegend")) {
            legend = (TLegend*)obj;
            break;
        }
    }

    return legend;
}

std::vector<TH1*> GetHistogramListFromCurrentPad()
{
    std::vector<TH1*> histograms;

    TObject *obj = NULL;
    TIter Next(gPad->GetListOfPrimitives());
    while ((obj = Next())) {
        if (obj->InheritsFrom("TH1")) {
            histograms.push_back((TH1*) obj);
        }
    }

    return histograms;
}


void SaveAllCanvasesAsPDF(TFile *f)
{
    // Loop through each canvas and save it as PDF
    TIter next(gROOT->GetListOfCanvases());
    TObject *obj;

    TDirectory *gParentDirectory = gDirectory;

    f->cd("/");
    if(f) {
        if(f->GetDirectory("canvas") == nullptr) f->mkdir("canvas");
        f->cd("canvas");
    }

    while ((obj = next())) {

        if (obj->IsA() == TCanvas::Class()) {

            if(f) {

                TCanvas *c = (TCanvas*) obj;
                c->Write();
            }
        }
    }

    gParentDirectory->cd();
}

int main(int argc, char **argv)
{
    gStyle->SetOptStat(0);

    // Parse command-line options
    TString precision = "";
    TString direction = "";
    TString buffer = "";
    TString type = "";
    TString outputFileName = "fft-benchmark.root";

    int opt;
    while ((opt = getopt(argc, argv, "p:d:b:t:o:")) != -1)
    {
        switch (opt)
        {
        case 'p':
            precision = TString(optarg);
            break;
        case 'd':
            direction = TString(optarg);
            break;
        case 'b':
            buffer = TString(optarg);
            break;
        case 't':
            type = TString(optarg);
            break;
        case 'o':
            outputFileName = TString(optarg);
            break;
        default:
            std::cerr << "Usage: " << argv[0] << " [-p precision] [-d direction] [-b buffer] [-t type] [-o output_filename] file1.json file2.json ... fileN.json" << std::endl;
            return 1;
        }
    }

    std::vector<std::string> files;
    
    for (int i = optind; i < argc; ++i) {
        files.push_back(argv[i]);
    }
    
    TApplication app("app", &argc, argv); // Initialize TApplication
    std::vector<Color_t> kColors = {kBlue+2, kRed, kGreen+2, kOrange, kViolet};
    
    // Extract benchmark information
    std::vector<Benchmark*> benchmarks;
    for (const auto &file : files)
    {
        std::ifstream ifs(file);
        TJson root;
        ifs >> root;

        Benchmark *benchmark = new Benchmark(root, kColors[benchmarks.size()]);
        for(auto *_benchmark : benchmarks) {

            if (_benchmark->IsCompatible(*benchmark)) {
                
                _benchmark->Print();
                _benchmark->AddEntry(*benchmark);
                _benchmark->Print();

                delete benchmark;
                benchmark = NULL;

                break;
            }
        }

        if(benchmark) benchmarks.push_back(benchmark);
    }
    
    double nFlopsMax = 0;
    double avgTimeMax = 0;
    double bestTimeMax = 0;
    double nCallsMax = 0;

    TFile *f = TFile::Open(outputFileName, "RECREATE");
    if(f) f->mkdir("histos");

    TLegend *legend = NULL;
    for(auto *benchmark : benchmarks) {

        benchmark->Print();
        for(auto *entry : benchmark->GetEntries()) {

            if(!direction.EqualTo("") && !direction.EqualTo(entry->GetDirection())) continue;
            if(!type.EqualTo("") && !type.EqualTo(entry->GetType())) continue;
            if(!buffer.EqualTo("") && !buffer.EqualTo(entry->GetBuffer())) continue;
            if(!precision.EqualTo("") && !precision.EqualTo(entry->GetPrecision())) continue;

            if(entry->GetN() < 1) {
                std::cerr << entry->GetTitle() << ": empty entry.. skip." << std::endl; 
                continue;
            }

            if(f->GetDirectory("histos/" + entry->GetTitle()) == nullptr) f->mkdir("histos/" + entry->GetTitle());
            f->cd("histos/" + entry->GetTitle());

            TCanvas *c = (TCanvas *) gROOT->GetListOfCanvases()->FindObject(entry->GetTitle());
            Option_t *opt = "SAME CP";
            if(c == NULL) {
                c = new TCanvas(entry->GetTitle(), entry->GetTitle(), 1200, 1200);
                if(extendPlots) c->Divide(2,2);
                else c->Divide(1,1);

                opt = "CP";
            }

            c->cd(1)->SetGridx(true);
            c->cd(1)->SetGridy(true);

            TH1D *hMFlops = (TH1D*) entry->GetMFlops(benchmark->GetColor());
            if(hMFlops) {

                nFlopsMax = TMath::Max(nFlopsMax, 1.45 * hMFlops->GetMaximum());
                hMFlops->GetYaxis()->SetRangeUser(0, nFlopsMax);
                if(hMFlops) hMFlops->Draw(opt);
                for( auto* h : GetHistogramListFromCurrentPad() ) {
                    h->GetYaxis()->SetRangeUser(1, nFlopsMax);
                }

                double binContentAverage = hMFlops->Integral()/hMFlops->GetNbinsX();
                legend = GetLegendFromCurrentPad();
                if(legend == NULL) legend = new TLegend(0.125,0.7,0.875,0.875);
                legend->AddEntry(hMFlops, entry->GetLibrary() + "(" + entry->GetCPU() + "); <Y> = "+ binContentAverage, "CP");
                legend->Draw();
            }

            if(extendPlots) {

                c->cd(2)->SetGridx(true);
                c->cd(2)->SetGridy(true);
                c->cd(2)->SetLogy(entry->GetMaxSize() > 16);

                TH1D *hAvgTime = (TH1D*) entry->GetAvgTime(benchmark->GetColor());
                if(hAvgTime) {

                    avgTimeMax = TMath::Max(avgTimeMax, 1.45 * hAvgTime->GetMaximum());
                    if(hAvgTime) hAvgTime->Draw(opt);
                    for( auto* h : GetHistogramListFromCurrentPad() ) {
                        h->GetYaxis()->SetRangeUser(1e-3, avgTimeMax);
                    }

                    legend = GetLegendFromCurrentPad();
                    if(legend == NULL) legend = new TLegend(0.125,0.7,0.875,0.875);
                    legend->AddEntry(hAvgTime, entry->GetLibrary() + "(" + entry->GetCPU() + ")", "CP");
                    legend->Draw();
                }

                c->cd(3)->SetGridx(true);
                c->cd(3)->SetGridy(true);
                c->cd(3)->SetLogy(entry->GetMaxSize() > 16);

                TH1D *hBestTime = (TH1D*) entry->GetBestTime(benchmark->GetColor());
                if(hBestTime) {

                    bestTimeMax = TMath::Max(bestTimeMax, 1.45 * hBestTime->GetMaximum());
                    if(hBestTime) hBestTime->Draw(opt);
                    for( auto* h : GetHistogramListFromCurrentPad() ) {
                        h->GetYaxis()->SetRangeUser(1e-3, bestTimeMax);
                    }

                    legend = GetLegendFromCurrentPad();
                    if(legend == NULL) legend = new TLegend(0.125,0.7,0.875,0.875);
                    legend->AddEntry(hBestTime, entry->GetLibrary() + "(" + entry->GetCPU() + ")", "CP");
                    legend->Draw();    
                }

                c->cd(4)->SetGridx(true);
                c->cd(4)->SetGridy(true);

                TH1D *hNCalls = (TH1D*) entry->GetNCalls(benchmark->GetColor());
                if(hNCalls) {

                    nCallsMax = TMath::Max(nCallsMax, 1.45 * hNCalls->GetMaximum());
                    if(hNCalls) hNCalls->Draw(opt);
                    for( auto* h : GetHistogramListFromCurrentPad() ) {
                        h->GetYaxis()->SetRangeUser(1, nCallsMax);
                    }

                    legend = GetLegendFromCurrentPad();
                    if(legend == NULL) legend = new TLegend(0.125,0.7,0.875,0.875);
                    legend->AddEntry(hNCalls, entry->GetLibrary() + "(" + entry->GetCPU() + ")", "CP");
                    legend->Draw();
                }
            }
    
            c->Update();
            c->Modified();
            f->cd("/");
        }
    }

    SaveAllCanvasesAsPDF(f);
    if(f) f->Write();

    std::cout << "Processing ended. Result stored in `" << outputFileName << "`" << std::endl;
    app.Run(); // Enter the event loop

    for(auto *_benchmark : benchmarks) {    
        delete _benchmark;
    }

    benchmarks.clear();

    return 0;
}
